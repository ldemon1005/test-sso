module.exports = {
    lintOnSave: false,
    runtimeCompiler: true,
    transpileDependencies: [/[\\\/]node_modules[\\\/]quasar-framework[\\\/]/],
    outputDir: "cordova/www",
    publicPath: ''
}
